@extends('layouts.app')

@section('content')
    <div class="container">
        <div class = "row">
            <div class = "col col-md-8">
@foreach($data['posts'] as $item)
            <h2><a href = "{{$item->url}}">{{$item->title}}</a></h2>
            <p>{!! $item->description !!}</p>
    @endforeach
            </div>
            <div class = "col col-md-4">
                @foreach($data['cats'] as $cat)
                    <p><a href="{{$cat->category}}">{{$cat->category}}</a></p>
                    @endforeach
            </div>
        </div>
    </div>
@endsection
