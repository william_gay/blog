<?php

namespace App\Http\Controllers;
use App\Post;
use App\BlogCategory;
use Illuminate\Http\Request;

class PagesController extends Controller
{
//    public function index(){
//      return view('index');
//    }
    public function index() {
        $posts = Post::where('id', '>', 0)->paginate(3);
        $cats = BlogCategory::all();

        $posts->setPath('blog');

        $data['posts'] = $posts;
        $data['cats'] = $cats;
       // dd($data);
        return view('blog', array('data' => $data, 'title' => 'Latest Blog Posts', 'description' => '', 'page' => 'blog'));
    }
    public function show($url) {
        //dd($url);
        $post = Post::whereUrl($url)->first();

        $tags = $post->tags;
        $prev_url = Post::prevBlogPostUrl($post->id);
        $next_url = Post::nextBlogPostUrl($post->id);
        $title = $post->title;
        $description = $post->description;
        $page = 'blog';
       // $brands = $this->brands;
       // $categories = $this->categories;
       // $products = $this->products;

        $data = compact('prev_url', 'next_url', 'tags', 'post', 'title', 'description', 'page',);

        return view('blog_post', $data);
    }
}
